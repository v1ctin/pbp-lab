from django.db import models


class Friend(models.Model):
    name = models.CharField(max_length=40)
    npm = models.CharField(max_length=20)
    DOB = models.DateField()
