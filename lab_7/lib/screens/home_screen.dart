import 'package:flutter/material.dart';

import '../widgets/main_drawer.dart';
import '../screens/login_screen.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Home',
              style: TextStyle(
                fontWeight: FontWeight.w200,
                fontSize: 24,
                color: Color.fromRGBO(95, 69, 31, 1),
              )),
        ),
        drawer: MainDrawer(),
        body: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
                Padding(padding: EdgeInsets.all(100)),
                Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text('Welcome to Mutuals',
                          style: TextStyle(
                            fontSize: 32,
                            color: Color.fromRGBO(0, 0, 0, 1),
                          )),
                      Image.asset(
                        'assets/images/Waving_Hand_Sign_Emoji_large.webp',
                        width: 40,
                      )
                    ]),
                Text(
                  '\nMutuals is a web application that focuses in connecting end user\nto a diverse group of people sharing the same interests. You may\nmessage them through the message box feature, or have fun with our\nshuffle button and communicate with randomly selected users\nbased on your shared interests.\n\n',
                  style: TextStyle(
                    color: Color.fromRGBO(0, 0, 0, 1),
                  ),
                  textAlign: TextAlign.center,
                ),
                Text(
                  "Haven't created an account?\n",
                  style: TextStyle(
                    color: Color.fromRGBO(0, 0, 0, 1),
                  ),
                  textAlign: TextAlign.center,
                ),
                ElevatedButton(
                    child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          "Let's Be Mutuals",
                        )),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => LoginScreen()));
                    })
              ])
            ]));
  }
}
