import 'package:flutter/material.dart';

import './screens/home_screen.dart';
import 'screens/edit_profile_screen.dart';

void main() => runApp(MutualsApp());

class MutualsApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MutualsApp> {
  Map<String, bool> _filters = {};

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mutuals',
      theme: ThemeData(
        primarySwatch: Colors.yellow,
        canvasColor: Color.fromRGBO(255, 255, 255, 1),
        fontFamily: 'Roboto',
        textTheme: ThemeData.light().textTheme.copyWith(
            bodyText1: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            bodyText2: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            headline6: TextStyle(
              fontSize: 20,
              fontFamily: 'RobotoCondensed',
              fontWeight: FontWeight.bold,
            )),
      ),
      // home: CategoriesScreen(),
      initialRoute: '/', // default is '/'
      routes: {
        '/': (ctx) => HomeScreen(),
        '/edit-profile': (ctx) => EditProfileScreen()
      },
      onGenerateRoute: (settings) {
        print(settings.arguments);
      },
    );
  }
}
