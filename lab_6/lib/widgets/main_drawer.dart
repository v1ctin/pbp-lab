import 'package:flutter/material.dart';
import '../screens/edit_profile_screen.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, VoidCallback tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: Color.fromRGBO(255, 235, 59, 1),
            child: Image.asset('assets/images/mutuals_logo.png'),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Home', Icons.house, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          buildListTile('Edit Profile', Icons.settings, () {
            Navigator.of(context)
                .pushReplacementNamed(EditProfileScreen.routeName);
          }),
        ],
      ),
    );
  }
}
