import 'package:flutter/material.dart';

import '../widgets/main_drawer.dart';

class EditProfileScreen extends StatefulWidget {
  static const routeName = '/edit-profile';
  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  @override
  bool isChecked = false;
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Edit Profile',
              style: TextStyle(
                fontWeight: FontWeight.w200,
                fontSize: 24,
                color: Color.fromRGBO(95, 69, 31, 1),
              )),
        ),
        drawer: MainDrawer(),
        body: Center(
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
            Padding(padding: EdgeInsets.all(20)),
            Image.asset('assets/images/Hello.png', height: 120),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    width: 210,
                    child: Expanded(
                        child: TextField(
                            decoration: const InputDecoration(
                      labelText: 'Username',
                    )))),
                Padding(padding: EdgeInsets.all(20)),
                Container(
                    width: 210,
                    child: Expanded(
                        child: TextField(
                      decoration: const InputDecoration(labelText: 'Password'),
                    )))
              ],
            ),
            Padding(padding: EdgeInsets.all(20)),
            Container(
                width: 460,
                child: Expanded(
                    child: TextField(
                        decoration: const InputDecoration(
                  labelText: 'Email',
                )))),
            Padding(padding: EdgeInsets.all(20)),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    width: 210,
                    child: Expanded(
                        child: TextField(
                            decoration: const InputDecoration(
                      labelText: 'Full Name',
                    )))),
                Padding(padding: EdgeInsets.all(20)),
                Container(
                    width: 210,
                    child: Expanded(
                        child: TextField(
                      decoration: const InputDecoration(labelText: 'Domicile'),
                    )))
              ],
            ),
            Padding(padding: EdgeInsets.all(20)),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    width: 210,
                    child: Expanded(
                        child: TextField(
                            decoration: const InputDecoration(
                      labelText: 'Line ID',
                    )))),
                Padding(padding: EdgeInsets.all(20)),
                Container(
                    width: 210,
                    child: Expanded(
                        child: TextField(
                      decoration: const InputDecoration(labelText: 'Instagram'),
                    )))
              ],
            ),
            Padding(padding: EdgeInsets.all(20)),
            Text('Your Interests (Leave empty to keep your current interests)'),
            Container(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Fantasy')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Romance')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Thrillers')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Mystery')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Comedy')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Documentary')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Comic')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Fashion')
                    ]),
                  ],
                ),
                Padding(padding: EdgeInsets.all(5)),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Soccer')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Basketball')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Swimming')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Gaming')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Reading')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Cooking')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Design')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Music')
                    ]),
                  ],
                ),
                Padding(padding: EdgeInsets.all(5)),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Food')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Drinks')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('School')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Teaching')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Movies')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('K-Dramas')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('KPOP')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Technology')
                    ]),
                  ],
                ),
                Padding(padding: EdgeInsets.all(5)),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Painting')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Concerts')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Piano')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Guitar')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Makeup')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Research')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Dancing')
                    ]),
                    Row(children: [
                      Checkbox(
                          value: isChecked,
                          onChanged: (bool? value) {
                            setState(() {
                              isChecked = value!;
                            });
                          }),
                      Text('Singing')
                    ]),
                  ],
                )
              ],
            )),
            Padding(padding: EdgeInsets.all(10)),
            ElevatedButton(
                child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Text(
                      "Submit",
                    )),
                onPressed: () {})
          ]),
        ));
  }
}
