1. There are many differences between JSON and XML, JSON stands for JavaScript object notation and its main purpose is 
   to represent objects using a language based on JavaScript, while XML stands for Extensible markup language and is a 
   markup language that uses tag structure to represent data items.

2. Both XML and HTML are both markup languages, the main difference is that XML is the absolute standard markup language 
   that other markup languages are defined on, also unlike HTML, XML is not a presentation nor a programming language and
   is used for transferring data while HTML is mainly used for displaying data.